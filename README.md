# SockWebServer
Terribly written lightweight embedded web server using Sockets. It has some pretty terrible callback support that is subject to change 'soon'.

It has a very simple request header parser, so some webby things might fail.

It has next to zero error checking, if you try to break it intentionally. It will break.

This is a direct port of my WinsockWebServer.

Use at your own risk ;)

## Why?
I needed a lightweight embedded web server and couldn't find anything that suited my needs.


## Usage
Either compile the application and run standalone:

```
Sudo ./SockWebServer /var/www 80
```

Or include "Server.cpp" and "Server.h" in your project.

```c++
// callback example
void callback(void* classPtr, char* requestedFile)
{
	Server* s = reinterpret_cast<Server*>(classPtr);

	std::cout << "Callback was called, file requested: " << requestedFile << std::endl);

	// get file data
	fileType file = s->GetFile(requestedFile);

	// send data
	int bytesSent = SendData(s->clientSocket, file.data, file.size);

	// clean up
	delete(file.data);
}


// typical usage example
Server* server = new Server("/var/www", 80);

// callbacks are optional. If no callback is set, everything is handled automatically
server->SetCallback(callback);

server->Start();


// Do your own magical code


server->Kill();
```

## Contributors
broken_: https://twitter.com/br0ken_code

## License
SockWebServer is MIT Licensed.
