#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <iostream>
#include <vector>

typedef void(recCallback)(void* classPtr, char* requestedFile);

struct fileData
{
    char* data;
    unsigned long size;
};

class Server
{
public:
    Server(char* rootPath, int Port);
    int Start();
    void Kill();
    void SetCallback(void* filename);
    static fileData GetFile(char* filename);
    static int SendData(int sock, char* buff, int size);

    int listenSocket;
    int clientSocket;

    std::vector<int> sockets;

    static std::string ParseHeader(char* header, int headerlen);
    static std::string GenerateHeader(std::string filereq);

private:
    int GetLastError() { return error; }
    void SetError(int err) { error = err; }

    static void* ServerThread(void* classPtr);
    static void* ConnectionThread(void* socketptr);

    pthread_t serverThreadHandle;

    std::vector<pthread_t> threads;

    bool isAlive;
    char* rootDir;
    int port;

    recCallback* cb;

    int error;
};
